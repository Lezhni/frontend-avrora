$(document).ready(function() {

	if ($(window).height() > 730 && $(window).height() < 1100) {
		$('.index main').height($(window).height());
	}
	$(window).resize(function() {
		if ($(window).height() > 730 && $(window).height() < 1100) {
			$('.index main').height($(window).height());
		}
	});

	$('.to-top').click(function() {
		$('html, body').animate({
			scrollTop: 0
		}, 700);
		return false;
	});

	$("#filter-price-val").ionRangeSlider({
		type: 'double',
		min: 0,
		max: 50000,
		step: 100,
		postfix: ' $',
		hide_min_max: true
	});

	$('.hotel-add-photos img').click(function() {
		var src = $(this).attr('src');
		$('.hotel-main-photo').attr('src', src);
	});
});